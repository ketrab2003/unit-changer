﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnitChanger
{
    public partial class NameDialog : Form
    {
        public string name = null;

        public NameDialog()
        {
            InitializeComponent();
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            if(nameBox.Text == "")
            {
                MessageBox.Show("Nazwa nie może być pusta");
                return;
            }
            if (UnitComputingLogic.instance.TypeNames().Contains(nameBox.Text))
            {
                MessageBox.Show("Nazwa koliduje z innym typem");
                return;
            }
            this.name = nameBox.Text;
            this.Close();
        }
    }
}
