﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UnitChanger
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

    class Units
    {
        public string name;
        public List<string> keys;
        public List<double> values;

        public Units(string _name)
        {
            this.name = _name;
            this.keys = new List<string>();
            this.values = new List<double>();
        }

        public Units(string _name, List<string> _keys, List<double> _values)
        {
            this.name = _name;
            this.keys = _keys;
            this.values = _values;
        }

        public double ValueOf(string key)
        {
            if (!this.keys.Contains(key)) return 0;
            return this.values[this.keys.IndexOf(key)];
        }

        public string DefaultUnit()
        {
            if (this.keys.Count == 0) return null;
            return this.keys[0];
        }

        public double Change(string fromUnits, string toUnits, double value)
        {
            try
            {
                return value / this.ValueOf(toUnits) * this.ValueOf(fromUnits);
            }
            catch
            {
                return 0;
            }
        }
    }

    class UnitComputingLogic
    {
        const string unitsFileName = "units.json";

        public List<Units> unitTypes = new List<Units>();
        public int activeType = 0;

        // singleton pattern
        public static UnitComputingLogic instance = new UnitComputingLogic();
        private UnitComputingLogic() {
            // Add loading units from file
            this.LoadSavedUnits();
        }

        private void LoadSavedUnits()
        {
            try
            {
                this.LoadUnits();
                return;
            }
            catch { }

            // Initialize default units if no save present
            Units a = new Units("Długość");
            a.keys = new List<string>() { "m", "km", "cm", "mm" };
            a.values = new List<double>() { 1, 1000, 0.01, 0.00001 };
            this.unitTypes.Add(a);
            Units b = new Units("Masa");
            b.keys = new List<string>() { "kg", "t", "g", "mg" };
            b.values = new List<double>() { 1, 1000, 0.001, 0.000001 };
            this.unitTypes.Add(b);
            Units c = new Units("Czas");
            c.keys = new List<string>() { "s", "min", "h", "dni" };
            c.values = new List<double>() { 1, 60, 3600, 86400 };
            this.unitTypes.Add(c);
            this.ExportUnits();
        }

        // Return all type names in string array
        public string[] TypeNames()
        {
            List<string> typeNames = new List<string>();
            foreach (Units type in this.unitTypes)
            {
                typeNames.Add(type.name);
            }

            return typeNames.ToArray();
        }

        TextBox fromValue, toValue;
        ComboBox fromUnits, toUnits, unitChoser;
        Label fromMessage, toMessage;

        // Called when Form is initialized
        public void Configure(TextBox _fromValue, TextBox _toValue, 
                              ComboBox _fromUnits, ComboBox _toUnits,
                              Label _fromMessage, Label _toMessage,
                              ComboBox _unitChoser)
        {
            this.fromValue = _fromValue;
            this.toValue = _toValue;
            this.fromUnits = _fromUnits;
            this.toUnits = _toUnits;
            this.fromMessage = _fromMessage;
            this.toMessage = _toMessage;
            this.unitChoser = _unitChoser;

            this.fromMessage.Text = "";
            this.toMessage.Text = "";

            this.ConfigureComboBoxes();
        }

        public void ConfigureComboBoxes()
        {
            this.ConfigureUnitComboBoxes();

            // configure unit types
            this.unitChoser.Items.Clear();
            this.unitChoser.Items.AddRange(this.TypeNames());
            this.unitChoser.SelectedIndex = this.activeType;
        }

        private void ConfigureUnitComboBoxes()
        {
            this.fromUnits.Items.Clear();
            this.fromUnits.Items.AddRange(this.unitTypes[this.activeType].keys.ToArray());
            this.fromUnits.SelectedIndex = 0;

            this.toUnits.Items.Clear();
            this.toUnits.Items.AddRange(this.unitTypes[this.activeType].keys.ToArray());
            this.toUnits.SelectedIndex = 1;
        }

        // Load units from json file
        public void LoadUnits(string _filename=null)
        {
            if(_filename is null)
            {
                _filename = unitsFileName;
            }

            string textData = System.IO.File.ReadAllText(_filename);
            var array = JsonConvert.DeserializeObject(textData);

            List<Units> newUnits = ((JArray)array).Select(x => new Units(
                (string)x["name"],
                x["keys"].Value<JArray>().ToObject<List<string>>(),
                x["values"].Value<JArray>().ToObject<List<double>>()
            )).ToList();

            this.unitTypes.AddRange(newUnits);
        }

        // Export units to json file
        public void ExportUnits(string _filename=null)
        {
            if (_filename is null)
            {
                _filename = unitsFileName;
            }

            string textData = JsonConvert.SerializeObject(this.unitTypes);
            System.IO.File.WriteAllText(_filename, textData);
        }

        public void UpdateTypesEditor(ListBox _unitTypesList)
        {
            _unitTypesList.Items.Clear();
            _unitTypesList.Items.AddRange(this.TypeNames());
            _unitTypesList.SelectedIndex = 0;
        }

        string lastFromUnit="", lastToUnit="";

        // Called every time units type changed
        public void UpdateTypes()
        {
            this.fromValue.Text = "";
            this.activeType = this.unitChoser.SelectedIndex;
            this.ConfigureUnitComboBoxes();
        }

        // Called every time when input changes
        public void Update()
        {
            if(fromUnits.Text == toUnits.Text)
            {
                if(fromUnits.Text == lastFromUnit)
                {
                    fromUnits.Text = lastToUnit;
                }
                else if(toUnits.Text == lastToUnit)
                {
                    toUnits.Text = lastFromUnit;
                }
            }

            lastFromUnit = fromUnits.Text;
            lastToUnit = toUnits.Text;

            if(this.fromValue.Text == "")
            {
                // User didn't write anything
                this.toValue.Text = "";
                this.fromMessage.Text = "";
                return;
            }

            // Try to read value from user
            double fromVal;
            try
            {
                fromVal = double.Parse(this.fromValue.Text);
            }
            catch(Exception e)
            {
                this.toValue.Text = "";
                this.fromMessage.Text = e.Message;
                return;
            }

            if (fromVal < 0)
            {
                this.toValue.Text = "";
                this.fromMessage.Text = "Wartość ma być nieujemna";
                return;
            }

            // Translate to set units
            double toVal = this.unitTypes[this.activeType].Change(
                this.fromUnits.Text, 
                this.toUnits.Text, 
                fromVal);

            // Update values
            this.toValue.Text = String.Format("{0:F20}", toVal).TrimEnd(new char[] {'0'}).TrimEnd(new char[] { ',', '.' });
            this.fromMessage.Text = "";
            this.toMessage.Text = "";
        }

        // Rotate From field with To field
        public void Rotate()
        {
            int tempFromUnits = this.fromUnits.SelectedIndex;
            int tempToUnits = this.toUnits.SelectedIndex;
            string tempToVal = this.toValue.Text;
            this.fromUnits.SelectedIndex = tempToUnits;
            this.toUnits.SelectedIndex = tempFromUnits;
            this.fromValue.Text = tempToVal;
        }
    }
}
