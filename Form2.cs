﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnitChanger
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void UpdateList()
        {
            UnitComputingLogic.instance.UpdateTypesEditor(unitTypesList);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.UpdateList();
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            if (unitTypesList.SelectedIndex < 0) return;

            Form3 dialog = new Form3((string)unitTypesList.SelectedItem, unitTypesList.SelectedIndex);
            dialog.ShowDialog();
            this.UpdateList();
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (unitTypesList.Items.Count < 2)
            {
                MessageBox.Show("Musi istnieć co najmniej jeden typ jednostek");
                return;
            }

            if (unitTypesList.SelectedIndex < 0) return;
            if (UnitComputingLogic.instance.activeType == unitTypesList.SelectedIndex) UnitComputingLogic.instance.activeType = 0;
            UnitComputingLogic.instance.unitTypes.RemoveAt(unitTypesList.SelectedIndex);
            this.UpdateList();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            NameDialog nameDialog = new NameDialog();
            nameDialog.ShowDialog();
            if (nameDialog.name is null) return;

            Form3 dialog = new Form3(nameDialog.name);
            dialog.ShowDialog();
            this.UpdateList();
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            UnitComputingLogic.instance.ExportUnits();
            this.Close();
        }
    }
}
