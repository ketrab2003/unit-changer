﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnitChanger
{
    public partial class UnitDialog : Form
    {
        public string name, defaultUnit;
        public double value;
        private string[] blacklist;

        public UnitDialog(string _defaultUnit, string[] _blacklist, string _name=null, double _value=0)
        {
            this.defaultUnit = _defaultUnit;
            this.name = _name;
            this.value = _value;
            this.blacklist = _blacklist;
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            if(this.defaultUnit is null || this.defaultUnit == this.name)
            {
                this.defaultUnit = null;
                valueBox.Text = "1";
                valueBox.Enabled = false;
            }
            else
            {
                unitLabel.Text = "[" + this.defaultUnit + "]";
            }

            if(this.name is null)
            {
                this.Text = "Dodaj jednostkę";
            }
            else
            {
                nameBox.Text = this.name;
                valueBox.Text = String.Format("{0:F20}", this.value).TrimEnd(new char[] { '0' }).TrimEnd(new char[] { ',', '.' });
                this.Text = "Edytuj jednostkę";
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.name = null;
            this.Close();
        }

        private void nameBox_TextChanged(object sender, EventArgs e)
        {
            if(this.defaultUnit is null)
            {
                unitLabel.Text = "["+ nameBox.Text + "]";
            }
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            if (nameBox.Text == "" || valueBox.Text == "")
            {
                MessageBox.Show("Wejście nie może być puste");
                return;
            }

            if (this.blacklist.Contains(nameBox.Text))
            {
                MessageBox.Show("Nazwa koliduje z istniejącą jednostką");
                return;
            }

            try
            {
                this.name = nameBox.Text;
                this.value = double.Parse(valueBox.Text);
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message);
                return;
            }

            this.Close();
        }
    }
}
