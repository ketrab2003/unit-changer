﻿
namespace UnitChanger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fromValue = new System.Windows.Forms.TextBox();
            this.toValue = new System.Windows.Forms.TextBox();
            this.toUnits = new System.Windows.Forms.ComboBox();
            this.fromUnits = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromComment = new System.Windows.Forms.Label();
            this.toComment = new System.Windows.Forms.Label();
            this.rotateButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.unitsType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.opcjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editUnitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fromValue
            // 
            this.fromValue.Location = new System.Drawing.Point(21, 66);
            this.fromValue.Name = "fromValue";
            this.fromValue.Size = new System.Drawing.Size(126, 20);
            this.fromValue.TabIndex = 0;
            this.fromValue.TextChanged += new System.EventHandler(this.input_Changed);
            // 
            // toValue
            // 
            this.toValue.Location = new System.Drawing.Point(330, 67);
            this.toValue.Name = "toValue";
            this.toValue.ReadOnly = true;
            this.toValue.Size = new System.Drawing.Size(126, 20);
            this.toValue.TabIndex = 1;
            // 
            // toUnits
            // 
            this.toUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toUnits.FormattingEnabled = true;
            this.toUnits.Location = new System.Drawing.Point(462, 66);
            this.toUnits.Name = "toUnits";
            this.toUnits.Size = new System.Drawing.Size(88, 21);
            this.toUnits.TabIndex = 2;
            this.toUnits.SelectedIndexChanged += new System.EventHandler(this.input_Changed);
            // 
            // fromUnits
            // 
            this.fromUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromUnits.FormattingEnabled = true;
            this.fromUnits.Location = new System.Drawing.Point(153, 65);
            this.fromUnits.Name = "fromUnits";
            this.fromUnits.Size = new System.Drawing.Size(88, 21);
            this.fromUnits.TabIndex = 3;
            this.fromUnits.SelectedIndexChanged += new System.EventHandler(this.input_Changed);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.opcjeToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(577, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "&Plik";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.closeToolStripMenuItem.Text = "&Zamknij";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // fromComment
            // 
            this.fromComment.AutoSize = true;
            this.fromComment.ForeColor = System.Drawing.Color.OrangeRed;
            this.fromComment.Location = new System.Drawing.Point(18, 89);
            this.fromComment.Name = "fromComment";
            this.fromComment.Size = new System.Drawing.Size(29, 13);
            this.fromComment.TabIndex = 5;
            this.fromComment.Text = "Error";
            // 
            // toComment
            // 
            this.toComment.AutoSize = true;
            this.toComment.ForeColor = System.Drawing.Color.OrangeRed;
            this.toComment.Location = new System.Drawing.Point(327, 90);
            this.toComment.Name = "toComment";
            this.toComment.Size = new System.Drawing.Size(29, 13);
            this.toComment.TabIndex = 6;
            this.toComment.Text = "Error";
            // 
            // rotateButton
            // 
            this.rotateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.rotateButton.Location = new System.Drawing.Point(266, 57);
            this.rotateButton.Name = "rotateButton";
            this.rotateButton.Size = new System.Drawing.Size(33, 34);
            this.rotateButton.TabIndex = 7;
            this.rotateButton.Text = "🔄";
            this.rotateButton.UseVisualStyleBackColor = true;
            this.rotateButton.Click += new System.EventHandler(this.rotateButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(250, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = ">";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = ">";
            // 
            // unitsType
            // 
            this.unitsType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.unitsType.FormattingEnabled = true;
            this.unitsType.Location = new System.Drawing.Point(116, 39);
            this.unitsType.Name = "unitsType";
            this.unitsType.Size = new System.Drawing.Size(126, 21);
            this.unitsType.TabIndex = 10;
            this.unitsType.SelectedIndexChanged += new System.EventHandler(this.unitsType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Rodzaj jedonstek:";
            // 
            // opcjeToolStripMenuItem
            // 
            this.opcjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editUnitsToolStripMenuItem});
            this.opcjeToolStripMenuItem.Name = "opcjeToolStripMenuItem";
            this.opcjeToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.opcjeToolStripMenuItem.Text = "&Konfiguracja";
            // 
            // editUnitsToolStripMenuItem
            // 
            this.editUnitsToolStripMenuItem.Name = "editUnitsToolStripMenuItem";
            this.editUnitsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.editUnitsToolStripMenuItem.Text = "&Edytuj jednostki";
            this.editUnitsToolStripMenuItem.Click += new System.EventHandler(this.editUnitsToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.infoToolStripMenuItem.Text = "&Info";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutToolStripMenuItem.Text = "&O programie";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 123);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.unitsType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rotateButton);
            this.Controls.Add(this.toComment);
            this.Controls.Add(this.fromComment);
            this.Controls.Add(this.fromUnits);
            this.Controls.Add(this.toUnits);
            this.Controls.Add(this.toValue);
            this.Controls.Add(this.fromValue);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zamieniacz jednostek";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fromValue;
        private System.Windows.Forms.TextBox toValue;
        private System.Windows.Forms.ComboBox toUnits;
        private System.Windows.Forms.ComboBox fromUnits;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.Label fromComment;
        private System.Windows.Forms.Label toComment;
        private System.Windows.Forms.Button rotateButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox unitsType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem opcjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editUnitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

