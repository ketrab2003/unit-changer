﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnitChanger
{
    public partial class Form3 : Form
    {
        Units activeType;
        bool newlyCreated = false;

        public Form3(string _name, int _chosenTypeIndex=-1)
        {
            if(_chosenTypeIndex >= 0) this.activeType = UnitComputingLogic.instance.unitTypes[_chosenTypeIndex];
            else
            {
                this.activeType = new Units(_name);
                this.newlyCreated = true;
            }
            InitializeComponent();
        }

        private void UpdateUnits()
        {
            nameList.Items.Clear();
            valueList.Items.Clear();

            nameList.Items.AddRange(this.activeType.keys.ToArray());
            foreach (double unit in this.activeType.values)
            {
                valueList.Items.Add(unit.ToString() + " [" + this.activeType.DefaultUnit() + "]");
            }

            if (nameList.Items.Count < 3)
            {
                deleteBtn.Enabled = false;
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.Text = this.activeType.name + " - edycja";
            editBtn.Enabled = false;
            deleteBtn.Enabled = false;
            this.UpdateUnits();
        }

        private void unitList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(nameList.SelectedIndex == 0 || nameList.Items.Count < 3)
                // Cannot remove default unit
                deleteBtn.Enabled = false;
            else
                deleteBtn.Enabled = true;

            if (nameList.SelectedIndex == -1)
                editBtn.Enabled = false;
            else
                editBtn.Enabled = true;
        }

        private void nameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            valueList.SelectedIndex = nameList.SelectedIndex;
            this.unitList_SelectedIndexChanged(sender, e);
        }

        private void valueList_SelectedIndexChanged(object sender, EventArgs e)
        {
            nameList.SelectedIndex = valueList.SelectedIndex;
            this.unitList_SelectedIndexChanged(sender, e);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (nameList.SelectedIndex < 0) return;

            this.activeType.keys.RemoveAt(nameList.SelectedIndex);
            this.activeType.values.RemoveAt(nameList.SelectedIndex);
            editBtn.Enabled = false;
            this.UpdateUnits();
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            if (nameList.SelectedIndex < 0) return;

            UnitDialog dialog = new UnitDialog(this.activeType.DefaultUnit(),
                                     this.activeType.keys.ToArray(),
                                     (string)nameList.SelectedItem,
                                     this.activeType.ValueOf((string)nameList.SelectedItem));
            dialog.ShowDialog();

            if(!(dialog.name is null))
            {
                this.activeType.keys[nameList.SelectedIndex] = dialog.name;
                this.activeType.values[nameList.SelectedIndex] = dialog.value;
                this.UpdateUnits();
            }
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            UnitDialog dialog = new UnitDialog(this.activeType.DefaultUnit(), this.activeType.keys.ToArray());
            dialog.ShowDialog();

            if (!(dialog.name is null))
            {
                this.activeType.keys.Add(dialog.name);
                this.activeType.values.Add(dialog.value);
                this.UpdateUnits();
            }
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            if(this.activeType.keys.Count < 2)
            {
                MessageBox.Show("Wymaga co najmniej dwóch jednostek");
                return;
            }

            if (newlyCreated)
            {
                UnitComputingLogic.instance.unitTypes.Add(this.activeType);
            }

            this.Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
