# Zamieniacz jednostek
Program pozwalający na dowolne zamienianie jednostek.

## Używanie
Wybierz odpowiedni typ jednostek (np.: Odległość lub Masa), 
dobierz jednostki źródłowe i docelowe oraz wpisz wartość do konwersji.

### Dodatkowe funkcjie
- Program pozwala na dodawanie własnych jednostek (opcja w menu konfiguracji)

## Autorzy
 - Bartek K  (*[ketrab2003](https://gitlab.com/ketrab2003)*)
