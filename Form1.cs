﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnitChanger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UnitComputingLogic.instance.Configure(fromValue, toValue, 
                                                  fromUnits, toUnits, 
                                                  fromComment, toComment,
                                                  unitsType);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void input_Changed(object sender, EventArgs e)
        {
            UnitComputingLogic.instance.Update();
        }

        private void rotateButton_Click(object sender, EventArgs e)
        {
            UnitComputingLogic.instance.Rotate();
        }

        private void unitsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UnitComputingLogic.instance.UpdateTypes();
        }

        private void editUnitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2();
            newForm.ShowDialog();
            UnitComputingLogic.instance.ConfigureComboBoxes();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IntorductionForm about = new IntorductionForm();
            about.ShowDialog();
        }
    }
}
